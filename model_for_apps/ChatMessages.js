function ConnectionSrvc(m, socket) {

    var recivedMessages = m.prop([]);

    function updateMessages(newMessages) {
        recivedMessages(newMessages);
        m.redraw();
    }

    function updateView() {
        return recivedMessages();
    }

    function putMsg(msgContent) {

        if(msgContent === '' || msgContent === null) {
            throw new Error('Please type a message first!');
        }

        var tweet = {
            user: 'Stefan', //hardcoded
            msg: msgContent
        };

        socket.emit('tweet', tweet);
    }

    return {
        updateView: updateView,
        updateMessages: updateMessages,
        putMsg: putMsg
    };
}

module.exports = ConnectionSrvc;
