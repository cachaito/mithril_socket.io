'use strict';

var server  = require('http').createServer(),
    io      = require('socket.io')(server),
    port    = 8080,
    id      = 3;

var temp = [//mocked
    {
        id: 1,
        user: "Julek",
        msg: "Hej ho, hej ho!"
    },
    {
        id: 2,
        user: "Stefan",
        msg: "Jestem!"
    }
];

io.on('connection', handleClient);

function handleClient(socket) {
    socket.emit('currentList', temp);
    socket.on('tweet', handleTweet);
}

function handleTweet(tweet) {
    var tweet = tweet;
    tweet.id = id++
    temp.push(tweet);
    io.emit('currentList', temp);
}

server.listen(port, function() {
    console.log('listening on * ' + port);
});
