'use strict';
var m           = require('mithril'),
    socket      = io.connect('http://localhost:8080'),
    initSrvc    = require('../../model_for_apps/ChatMessages')(m, socket);

console.log('Hello from App2!');

var socket = io.connect('http://localhost:8080');

socket.on('connect', function() {
    console.log('Connected!');
});

socket.on('currentList', function(newList) {
    initSrvc.updateMessages(newList);
});

var ChatRead = {
    vm: {
        messages: initSrvc.updateView
    },
    controller: function() {},
    view: function() {
        return m('form', [
            m('fieldset', [
                m('ul', ChatRead.vm.messages().map(function(elem) {
                    return m('li', [
                        m('span', elem.id),
                        m('span', elem.user),
                        m('span', elem.msg)
                    ]);
                })),
                m('legend', 'Chat window')
            ])
        ]) ;
    }
};

m.mount(document.body, ChatRead);
