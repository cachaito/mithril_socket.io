'use strict';
var m           = require('mithril'),
    socket      = io.connect('http://localhost:8080'),
    initSrvc    = require('../../model_for_apps/ChatMessages')(m, socket);

console.log('Hello from App1!');

socket.on('connect', function() {
    console.log('Connected!');
});

socket.on('currentList', function(newList) {
    initSrvc.updateMessages(newList);
});

// function ConnectionSrvc() {

//     var recivedMessages = m.prop([]);

//     function updateMessages(newMessages) {
//         recivedMessages(newMessages);
//         m.redraw();
//     }

//     function updateView() {
//         return recivedMessages();
//     }

//     function putMsg(msgContent) {

//         if(msgContent === '' || msgContent === null) {
//             throw new Error('Please type a message first!');
//         }

//         var tweet = {
//             user: 'Stefan', //hardcoded
//             msg: msgContent
//         };

//         socket.emit('tweet', tweet);
//     }

//     return {
//         updateView: updateView,
//         updateMessages: updateMessages,
//         putMsg: putMsg
//     };
// }

// var initSrvc = ConnectionSrvc(); //jesli skorzystalibysmy z modulu, nie trzeba by trzymac stanu w initSrvc

var ChatWrite = {
    vm: {
        messages: initSrvc.updateView,
        newEntry: m.prop(null),
        saveMsg: function(e) {
            e.preventDefault();
            initSrvc.putMsg(ChatWrite.vm.newEntry()); //save to socket server
        }
    },
    controller: function() {},
    view: function() {
        return m('form', {onsubmit: ChatWrite.vm.saveMsg}, [
            m('fieldset', [
                m('ul', ChatWrite.vm.messages().map(function(elem) {
                    return m('li', [
                        m('span', elem.id),
                        m('span', elem.user),
                        m('span', elem.msg)
                    ]);
                })),
                m('legend', 'Chat window'),
                m('input', {placeholder: 'Enter text', onchange: m.withAttr('value', ChatWrite.vm.newEntry)}),
                m('input', {type: 'submit'}, 'Send')
            ])
        ]) ;
    }
};

m.mount(document.body, ChatWrite);
